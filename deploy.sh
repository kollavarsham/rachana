#!/usr/bin/env bash

FONT_NAME="Rachana"
GITLAB_URL="https://smcwebfonts-bot:${GITLAB_REPO_TOKEN}@gitlab.com/kollavarsham/smc-webfonts.git"

# clone, add updated fonts, commit and push to smc-webfonts repo
git config --global user.name "SMC Webfonts Bot"
git config --global user.email "smc-webfonts@kollavarsham.org"
git clone ${GITLAB_URL} .smc-webfonts > /dev/null 2>&1
cd .smc-webfonts
cp -v ../*.{otf,ttf,woff,woff2} fonts/${FONT_NAME}/
git add fonts/${FONT_NAME}/
git commit -m "update '${FONT_NAME}' fonts"
git push origin master > /dev/null 2>&1
